const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const bannerImage = sequelize.define('bannerimage', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    assetUrl: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
    text: {
        type: Sequelize.STRING,
    },
    color: {
        type: Sequelize.STRING,
    },
    priority: {
        type: Sequelize.INTEGER,
    },
    url: {
        type: Sequelize.STRING,
    },
});

bannerImage.sync();

module.exports = bannerImage;