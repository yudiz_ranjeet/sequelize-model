const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const reports = sequelize.define('reports', {
    userId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: false,
    },
    reportedUserId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: true,
    },
    reportedRentalId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'rentals',
            key: 'id',
        },
        allowNull: true,
    },
    text: {
        type: Sequelize.TEXT,
    },
    isBlocked: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

reports.sync();

module.exports = reports;