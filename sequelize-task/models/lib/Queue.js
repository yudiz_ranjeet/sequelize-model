const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const queue = sequelize.define('queue', {
    type: {
        type: Sequelize.ENUM,
        values: ['e', 'n'],
        allowNull: false,
    },
    data: {
        type: Sequelize.TEXT,
    },
    template: {
        type: Sequelize.STRING,
    },
});

queue.sync();

module.exports = queue;