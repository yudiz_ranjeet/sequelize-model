const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const splashImage = sequelize.define('splashImage', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    assetUrl: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    textColor: {
        type: Sequelize.STRING,
    },
    isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

splashImage.sync();

module.exports = splashImage;