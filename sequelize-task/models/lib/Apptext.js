const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const apptext = sequelize.define('apptext', {
    text: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
    priority: {
        type: Sequelize.INTEGER,
    },
});

apptext.sync();

module.exports = apptext;