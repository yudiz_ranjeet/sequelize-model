const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const rentalImage = sequelize.define('rentalImage', {
    rentalId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'rentals',
            key: 'id',
        },
        allowNull: false,
    },
    imageUrl: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
    priority: {
        type: Sequelize.INTEGER,
    },
});

rentalImage.sync();

module.exports = rentalImage;