const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const categories = sequelize.define('categories', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

categories.sync();

module.exports = categories;