const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const rentals = sequelize.define('rentals', {
    userId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: false,
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    description: {
        type: Sequelize.TEXT,
    },
    status: {
        type: Sequelize.ENUM,
        values: ['pending', 'rejected', 'approved'],
        defaultValue: 'pending',
    },
    originalPrice: {
        type: Sequelize.FLOAT,
        allowNull: false,
    },
    rentalPricePerWeek: {
        type: Sequelize.FLOAT,
        allowNull: false,
    },
    wasUpdated: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    deliveryPrice: {
        type: Sequelize.FLOAT,
    },
    dryCleanPrice: {
        type: Sequelize.FLOAT,
    },
    borrowerServiceFee: {
        type: Sequelize.FLOAT,
        allowNull: false,
    },
    loanerServiceFee: {
        type: Sequelize.FLOAT,
        allowNull: false,
    },
    hasDelivery: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
    needsDryCleaning: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    totalPrice: {
        type: Sequelize.FLOAT,
        allowNull: false,
    },
    isPickup: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
    isLook: {
        type: Sequelize.BOOLEAN,
    },
    isActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
    addressId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'addresses',
            key: 'id',
        },
        // allowNull: false,
    },
    shareUrl: {
        type: Sequelize.STRING,
    },
    isListed: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

rentals.sync();

module.exports = rentals;