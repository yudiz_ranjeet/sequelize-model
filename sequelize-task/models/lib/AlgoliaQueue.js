const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const algoliaQueue = sequelize.define('algoliaQueue', {
    data: {
        type: Sequelize.TEXT,
    },
});

algoliaQueue.sync();

module.exports = algoliaQueue;