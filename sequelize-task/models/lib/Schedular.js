const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const values = [
    'requestPending:a:12',
    'requestPending:a:23',
    'requestPending:a:24',
    'requestPending:b:06',
    'requestPending:b:24',
    'paymentPending:a:12',
    'paymentPending:a:23',
    'paymentPending:a:24',
    'paymentPending:b:24',
    'paymentPending:b:06',
    'paymentAuthorized:b:48',
    'deliveryWaitingForLoaner:b:72',
    'deliveryWaitingForLoaner:b:48',
    'deliveryWaitingForLoaner:b:24',
    'deliveryWaitingForLoaner:b:12',
    'deliveryWaitingForLoaner:b:06',
    'deliveryWaitingForLoaner:b:12:admin',
    'deliveryShippedByLoaner:a:00',
    'deliveryShippedByLoaner:a:03',
    'deliveryShippedByLoaner:a:04:admin',
    'inPersonCollectionPending:b:72',
    'inPersonCollectionPending:b:24',
    'inPersonCollectionPending:b:03',
    'inPersonCollectionPending:a:00',
    'inPersonCollectionPending:a:03',
    'inPersonCollectionPending:a:04:admin',
    'returnWaitingForBorrower:b:72',
    'returnWaitingForBorrower:b:48',
    'returnWaitingForBorrower:b:24',
    'returnWaitingForBorrower:b:12',
    'returnWaitingForBorrower:b:12:admin',
    'returnWaitingForBorrower:b:06',
    'returnWaitingForBorrower:a:3d',
    'returnShippedByBorrower:a:00',
    'returnShippedByBorrower:a:03',
    'returnShippedByBorrower:a:04:admin',
    'returnShippedByBorrower:a:24:admin',
    'inPersonReturnPending:b:72',
    'inPersonReturnPending:b:48',
    'inPersonReturnPending:b:24',
    'inPersonReturnPending:a:00',
    'inPersonReturnPending:a:03',
    'inPersonReturnPending:a:04:admin',
    'inPersonReturnPending:a:24:admin',
    'payout:a:48',
];
const schedular = sequelize.define('schedular', {
    rentalTransactionId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'rentalTransactions',
            key: 'id',
        },
    },
    executeOn: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
    },
    functionName: {
        type: Sequelize.ENUM,
        values,
    },
});

schedular.sync();

module.exports = schedular;