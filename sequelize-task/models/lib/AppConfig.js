const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const appConfig = sequelize.define('appConfig', {
    noFees: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
    borrowerItemGBP: Sequelize.FLOAT,
    borrowerLookGBP: Sequelize.FLOAT,
    loanerPercent: Sequelize.FLOAT, // in precentage
    retunRentalInDays: Sequelize.INTEGER,
    defaultSuggestPricePercent: Sequelize.FLOAT, // in precentage
    // app links
    TERMS_AND_CONDITION: Sequelize.STRING,
    PRIVACY: Sequelize.STRING,
    FAQ: Sequelize.STRING,
    STRIPE_CONNECT_FAQ: Sequelize.STRING,
    STRIPE_IDENTITY_FAQ: Sequelize.STRING,
    HOW_IT_WORKS: Sequelize.STRING,
    LOANER_GUIDE: Sequelize.STRING,
    BORROWER_GUIDE: Sequelize.STRING,
    WAITING_LIST_URL: Sequelize.STRING,
});

appConfig.sync();

module.exports = appConfig;