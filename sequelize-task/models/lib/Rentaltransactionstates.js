const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const values = [
    'requestPending',
    'requestDeclined',
    'requestCanceled',
    'requestAccepted',
    'requestTimout',
    'paymentPending',
    'paymentFailed',
    'paymentTimout',
    'paymentAuthorized',
    'paymentSuccess',
    'confirmed',
    'deliveryWaitingForLoaner',
    'deliveryShippedByLoaner',
    'inPersonCollectionPending',
    'receivedByBorrower',
    'extensionRequestPending',
    'extensionRequestDeclined',
    'extensionRequestCanceled',
    'extensionRequestAccepted',
    'extensionPaymentPending',
    'extensionPaymentFailed',
    'extensionPaymentSuccess',
    'extensionConfirmed',
    'returnWaitingForBorrower',
    'returnShippedByBorrower',
    'inPersonReturnPending',
    'receivedByLoaner',
    'completed',
];

const rentaltransactionstates = sequelize.define('rentaltransactionstates', {
    rentalTransactionId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'rentalTransactions',
            key: 'id',
        },
        allowNull: false,
    },
    state: {
        type: Sequelize.ENUM,
        values,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

rentaltransactionstates.sync();

module.exports = rentaltransactionstates;