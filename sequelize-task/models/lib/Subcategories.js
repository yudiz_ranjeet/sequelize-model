const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const subcategories = sequelize.define('subcategories', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    categoryId: {
        type: Sequelize.BIGINT,
        allowNull: false,
        references: {
            model: 'categories',
            key: 'id',
        },
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

subcategories.sync();

module.exports = subcategories;