const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');

const address = sequelize.define('address', {
    userId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: false,
    },
    address1: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    address2: {
        type: Sequelize.STRING,
    },
    city: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    postCode: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    isDelivery: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    isDropOff: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    isPickUp: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

address.sync();

module.exports = address;