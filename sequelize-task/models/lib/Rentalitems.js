const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const rentalitems = sequelize.define('rentalitems', {
    brandId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'brands',
            key: 'id',
        },
        allowNull: false,
    },
    colorId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'colors',
            key: 'id',
        },
        allowNull: false,
    },
    materialId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'materials',
            key: 'id',
        },
        allowNull: false,
    },
    rentalId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'rentals',
            key: 'id',
        },
        allowNull: false,
    },
    sizeId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'sizes',
            key: 'id',
        },
        allowNull: false,
    },
    subcategoryId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'subcategories',
            key: 'id',
        },
    },
    categoryId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'categories',
            key: 'id',
        },
    },
    careDuringRental: {
        type: Sequelize.ENUM,
        values: ['doNotClean', 'dryCleanOnly', 'handWashOnly', 'other'],
    },
    careOther: {
        type: Sequelize.STRING,
    },
    condition: {
        type: Sequelize.ENUM,
        values: ['new', 'good', 'excellent'],
    },
    isPetite: {
        type: Sequelize.BOOLEAN,
    },
    isTall: {
        type: Sequelize.BOOLEAN,
    },
    isCurve: {
        type: Sequelize.BOOLEAN,
    },
    isMaternity: {
        type: Sequelize.BOOLEAN,
    },
    careLabel: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

rentalitems.sync();

rentalitems.addHook('beforeSave', (rentalItem, options) => {
    const { condition } = rentalItem;
    rentalItem.condition = condition.toLowerCase();
});

module.exports = rentalitems;