const Sequelize = require('sequelize');

const sequelize = require('../../db/sequelize');

const accessCode = sequelize.define('accessCode', {
    aCode: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    maxUses: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    numberUsed: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
    },
    expiryDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

accessCode.sync();

module.exports = accessCode;