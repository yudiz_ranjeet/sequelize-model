const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const colors = sequelize.define('colors', {
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    uri: {
        type: Sequelize.STRING,
    },
    colorCode: {
        type: Sequelize.STRING,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

colors.sync();

module.exports = colors;