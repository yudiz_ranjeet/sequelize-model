const Sequelize = require('sequelize');
const sequelize = require('../../db/sequelize');
const follows = sequelize.define('follows', {
    userId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: false,
    },
    followerUserId: {
        type: Sequelize.BIGINT,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: false,
    },
    status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
    },
});

follows.sync();

module.exports = follows;