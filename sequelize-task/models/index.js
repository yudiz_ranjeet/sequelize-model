const User = require('./lib/User');
const Materials = require('./lib/Materials');
const Brands = require('./lib/Brands');
const Categories = require('./lib/Categories');
const Colors = require('./lib/Colors');
const Sizegroups = require('./lib/Sizegroups');
const Sizes = require('./lib/Sizes');
const Subcategories = require('./lib/Subcategories');
const Splashimages = require('./lib/Splashimages');
const Bannerimages = require('./lib/Bannerimages');
const Address = require('./lib/Address');
const AccessCode = require('./lib/AccessCode');
const Rentals = require('./lib/Rentals');
const Rentalitems = require('./lib/Rentalitems');
const Rentalimages = require('./lib/Rentalimages');
const Apptext = require('./lib/Apptext');
const Rentaltransactions = require('./lib/Rentaltransactions');
const Messages = require('./lib/Messages');
const Follows = require('./lib/Follows');
const Likes = require('./lib/Likes');
const Rentaltransactionstates = require('./lib/Rentaltransactionstates');
const Reports = require('./lib/Reports');
const Schedular = require('./lib/Schedular');
const Queue = require('./lib/Queue');
const AlgoliaQueue = require('./lib/AlgoliaQueue');
const AppConfig = require('./lib/AppConfig');

User.hasMany(Rentals, { onDelete: 'cascade' });
Rentals.belongsTo(User);

User.hasMany(Reports, { onDelete: 'cascade', foreignKey: 'userId', as: 'reporter' });
Reports.belongsTo(User, { foreignKey: 'userId', as: 'reporter' });

User.hasMany(Reports, { onDelete: 'cascade', foreignKey: 'reportedUserId', as: 'reported' });
Reports.belongsTo(User, { foreignKey: 'reportedUserId', as: 'reported' });

User.hasMany(Address, { onDelete: 'cascade' });
Address.belongsTo(User);

User.hasMany(Likes, { onDelete: 'cascade' });
Likes.belongsTo(User);

User.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'borrowerId', as: 'borrower' });
Rentaltransactions.belongsTo(User, { foreignKey: 'borrowerId', as: 'borrower' });

User.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'loanerId', as: 'loaner' });
Rentaltransactions.belongsTo(User, { foreignKey: 'loanerId', as: 'loaner' });

User.hasMany(Messages, { onDelete: 'cascade', foreignKey: 'senderId', as: 'sender' });
Messages.belongsTo(User, { foreignKey: 'senderId', as: 'sender' });

User.hasMany(Messages, { onDelete: 'cascade', foreignKey: 'receiverId', as: 'receiver' });
Messages.belongsTo(User, { foreignKey: 'receiverId', as: 'receiver' });

User.hasMany(Follows, { onDelete: 'cascade', foreignKey: 'userId', as: 'following' });
Follows.belongsTo(User, { foreignKey: 'userId', as: 'following' });

User.hasMany(Follows, { onDelete: 'cascade', foreignKey: 'followerUserId', as: 'follower' });
Follows.belongsTo(User, { foreignKey: 'followerUserId', as: 'follower' });

Brands.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Brands);

Categories.hasMany(Subcategories, { onDelete: 'cascade' });
Subcategories.belongsTo(Categories);

Categories.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Categories);

Colors.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Colors);

Materials.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Materials);


Sizegroups.hasMany(Sizes, { onDelete: 'cascade' });
Sizes.belongsTo(Sizegroups);

Sizes.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Sizes);

Address.hasMany(Rentals, { onDelete: 'cascade', foreignKey: 'addressId' });
Rentals.belongsTo(Address, { foreignKey: 'addressId' });

Address.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'borrowerAddressId', as: 'borrowerAddress' });
Rentaltransactions.belongsTo(Address, { foreignKey: 'borrowerAddressId', as: 'borrowerAddress' });

Address.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'loanerAddressId', as: 'loanerAddress' });
Rentaltransactions.belongsTo(Address, { foreignKey: 'loanerAddressId', as: 'loanerAddress' });

Follows.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'followerUserId', as: 'followingReported' });
Reports.belongsTo(Follows, { foreignKey: 'userId' });

Follows.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'followerUserId', as: 'followingReporter' });
Reports.belongsTo(Follows, { foreignKey: 'reportedUserId' });

Follows.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'userId', as: 'followerReported' });
Reports.belongsTo(Follows, { foreignKey: 'userId' });

Follows.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'userId', as: 'followerReporter' });
Reports.belongsTo(Follows, { foreignKey: 'reportedUserId' });

Rentals.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Rentals);

Rentals.hasMany(Reports, { onDelete: 'cascade', foreignKey: 'reportedRentalId' });
Reports.belongsTo(Rentals, { foreignKey: 'reportedRentalId' });

Rentals.hasMany(Rentalimages, { onDelete: 'cascade' });
Rentalimages.belongsTo(Rentals);

Rentals.hasMany(Rentaltransactions, { onDelete: 'cascade', foreignKey: 'rentalId' });
Rentaltransactions.belongsTo(Rentals, { foreignKey: 'rentalId' });

Rentals.hasMany(Messages, { onDelete: 'cascade' });
Messages.belongsTo(Rentals);

Rentals.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'userId' });
Follows.belongsTo(Rentals, { foreignKey: 'userId', sourceKey: 'userId' });

Rentals.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'userId', as: 'reporterUser' });
Reports.belongsTo(Rentals, { foreignKey: 'userId', sourceKey: 'userId', as: 'reporterUser' });

Rentals.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'userId', as: 'reportedUser' });
Reports.belongsTo(Rentals, { foreignKey: 'userId', as: 'reportedUser' });

Rentals.hasMany(Likes, { onDelete: 'cascade' });
Likes.belongsTo(Rentals);

Subcategories.hasMany(Rentalitems, { onDelete: 'cascade' });
Rentalitems.belongsTo(Subcategories);

Rentaltransactions.hasMany(Messages, { onDelete: 'cascade' });
Messages.belongsTo(Rentaltransactions);

Rentaltransactions.hasMany(Schedular, { onDelete: 'cascade' });
Schedular.belongsTo(Rentaltransactions);

Rentaltransactions.hasMany(Rentaltransactionstates, { onDelete: 'cascade' });
Rentaltransactionstates.belongsTo(Rentaltransactions);

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'loanerId', as: 'reportedLoaner' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'loanerId', as: 'reporterLoaner' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'userId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'borrowerId', as: 'reportedBorrower' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'borrowerId', as: 'reporterBorrower' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'userId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'loanerId', as: 'loanerReport' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Rentaltransactions.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'borrowerId', as: 'borrowerReport' });
Reports.belongsTo(Rentaltransactions, { foreignKey: 'reportedUserId' });

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'senderId', as: 'senderReport' });
Reports.belongsTo(Messages, { foreignKey: 'reportedUserId' });

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'receiverId', as: 'receiverReport' });
Reports.belongsTo(Messages, { foreignKey: 'reportedUserId' });


Messages.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'senderId', as: 'senderFollow' });
Follows.belongsTo(Messages, { foreignKey: 'userId', as: 'senderFollow' });

Messages.hasMany(Follows, { foreignKey: 'userId', sourceKey: 'receiverId', as: 'receiverFollow' });
Follows.belongsTo(Messages, { foreignKey: 'userId', as: 'receiverFollow' });

Messages.hasMany(Reports, { foreignKey: 'userId', sourceKey: 'receiverId' });
Reports.belongsTo(Messages, { foreignKey: 'userId', sourceKey: 'userId' });

Messages.hasMany(Reports, { foreignKey: 'reportedUserId', sourceKey: 'receiverId' });
Reports.belongsTo(Messages, { foreignKey: 'userId' });

module.exports = {
    User,
    Materials,
    Brands,
    Categories,
    Colors,
    Sizegroups,
    Sizes,
    Subcategories,
    Splashimages,
    Rentals,
    Rentalitems,
    Rentalimages,
    Address,
    AccessCode,
    Rentaltransactions,
    Rentaltransactionstates,
    Apptext,
    Bannerimages,
    Messages,
    Follows,
    Likes,
    Schedular,
    Reports,
    Queue,
    AlgoliaQueue,
    AppConfig,
};