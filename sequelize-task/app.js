const express = require('express')
const morgan = require('morgan');
const app = express()
app.use(morgan('dev'))
const sequelize = require('./db/sequelize')

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.get('/', (req, res) => {
    res.send("Home page")
})
const all = require('./routes/router')
app.use('/api', all)
    // sequelize.sync().then((req) => {
app.listen(8080, function() {
    console.log('example app listening on port 8080')
})