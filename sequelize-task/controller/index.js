const db = require('../models/index')
const Query1 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let data = db.Rentals.findAndCountAll({
            include: [{ model: db.Rentalimages },
                {
                    model: db.User,
                    attributes: ['id', 'firstname', 'lastname'],
                    required: true
                },
            ],
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        })
        return res.json(data)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query2 = (req, res, next) => {
    const { page = 1, limit = 10 } = req.query;
    try {
        let data = db.Rentals.findAndCountAll({
            include: [
                { model: db.Rentalitems },
                {
                    model: db.Rentalimages,
                    attributes: ['rentalId', 'imageUrl', 'status', ],
                    required: true
                },
                {
                    model: db.User,
                    attributes: ['id', 'firstname', 'lastname', ],
                    required: true
                },
            ],
            // offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        })
        return res.json(data)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query3 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let data = db.Rentals.findAndCountAll({
            include: [{
                model: db.Rentaltransactions,
                include: [{
                        model: db.User,
                        as: 'borrower',
                        required: true
                    },
                    {
                        model: db.User,
                        as: 'loaner',
                        required: true
                    }
                ],
            }],
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        });
        return res.json(data)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query4 = (req, res, next) => {
    const { limit = 10 } = req.query;
    try {
        const result = db.Rentals.findAndCountAll({
            include: [{
                model: db.Rentaltransactions,
                required: true,
                include: [{
                    model: db.User,
                    as: 'loaner'
                }]
            }],
            limit: 1
        })
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query6 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.Rentals.findAndCountAll({
            include: [{
                model: db.Reports,
                required: true,
                attributes: ["id", 'userId', 'text'],
                include: [{
                    model: db.User,
                    attributes: ["id", 'userName', 'emailId'],
                    require: true,
                    as: "reporter"
                }]
            }],
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        });
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query5 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        const result = db.User.findAndCountAll({
            include: [{
                model: db.Reports,
                attributes: ["id", 'userId', 'reportedUserId', "text"],
                required: true,
                as: 'reported'
            }],
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        }, );
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query7 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.Rentals.findAndCountAll({
            include: [{
                model: db.Rentaltransactions,
                attributes: ['id'],
                required: true,
                include: {
                    model: db.Rentaltransactionstates,
                    attributes: ['id', 'status'],
                    required: true
                }
            }],
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        });
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query8 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.Rentals.findAndCountAll({
            include: {
                model: db.Rentalitems,
                include: [
                    { model: db.Brands },
                    {
                        model: db.Categories,
                        include: [{
                            model: db.Subcategories
                        }]
                    },
                    { model: db.Sizes },
                ]
            },
            limit: 1
        })
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query9 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        const result = db.Rentals.findAndCountAll({
            include: {
                model: db.Rentaltransactions,
                include: [{ model: db.User, as: 'loaner' },
                    {
                        attributes: {
                            include: [
                                [Sequelize.fn('COUNT', Sequelize.col('rentalTransactions.borrowerId')), 'borrowerCount']
                            ]
                        },
                        model: User,
                        as: 'borrower'
                    },
                    { model: db.Rentaltransactionstates, attributes: ['state'], required: true }
                ]
            },
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit),
        })
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query10 = (req, res, next) => {}
const Query11 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.Messages.findAndCountAll({
            include: [{
                model: db.Rentals,
                attributes: ['userId', 'description', ],
                include: {
                    model: db.Rentaltransactions,
                    include: [{
                        model: db.User,
                        as: 'loaner',
                        model: db.User,
                        as: 'borrower'
                    }]
                },
            }],
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        });
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query12 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.Rentals.findAndCountAll({
            include: [
                { model: db.Brands },
                { model: db.Categories },
                { model: db.Subcategories },
                { model: db.Sizes },
                { model: db.User }
            ],
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        });

        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query13 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.Rentals.findAndCountAll({
            include: {
                model: db.Likes,
                attributes: ['id'],
                include: {
                    model: db.User,
                    attributes: ['id'],
                }
            },
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        });
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query14 = (req, res, next) => {}
const Query15 = (req, res, next) => {}
const Query16 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.User.findAndCountAll({
            include: {
                model: db.Reports,
                as: 'reported',
                required: true,
                attributes: ["id", "userId", "reportedUserId", "isActive"],
                where: { isActive: true }
            },
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        })
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query17 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.User.findAndCountAll({
            include: {
                model: db.Reports,
                as: 'reported',
                required: true,
                attributes: ['text']
            },

            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        })
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query18 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.User.findAndCountAll({
            include: {
                model: db.Rentals,
                include: { model: db.Reports, }
            },
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        });
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query19 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.User.findAndCountAll({
            include: {
                model: db.Reports,
                as: 'reported',
                where: { isBlocked: true },
                include: {
                    model: db.Rentals
                }
            },
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        })
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
const Query20 = (req, res, next) => {
    const { page = 10, limit = 10 } = req.query;
    try {
        let result = db.User.findAndCountAll({
            include: {
                model: db.Reports,
                as: 'reporter',
                where: { isBlocked: true },
                include: {
                    model: db.Rentals
                }
            },
            offset: parseInt((page - 1) * limit),
            limit: parseInt(limit)
        })
        return res.json(result)
    } catch (err) {
        res.status(400).json({
            message: "wrong"
        })
    }
}
module.exports = {
    Query1,
    Query2,
    Query3,
    Query4,
    Query5,
    Query6,
    Query7,
    Query8,
    Query9,
    Query10,
    Query11,
    Query12,
    Query13,
    Query14,
    Query15,
    Query16,
    Query17,
    Query18,
    Query19,
    Query20,
}